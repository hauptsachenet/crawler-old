<?php

namespace App\Controller;


use App\Crawler\Entity\Project;
use App\Crawler\Repository\ProjectRepository;
use App\Crawler\Repository\UrlRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends Controller
{
    /**
     * @Route(name="project_list", path="/")
     */
    public function listAction()
    {
        $list = $this->get(ProjectRepository::class)->createList();

        return $this->render('Project/list.html.twig', [
            'list' => $list
        ]);
    }

    /**
     * @Route(name="project", path="/project/{project}")
     */
    public function showAction(Project $project)
    {
        $urlRepository = $this->get(UrlRepository::class);

        $statusCriteria = function (int $from, int $to): Criteria {
            $criteria = Criteria::create();
            $criteria->where(Criteria::expr()->andX(
                Criteria::expr()->gte('result.status', $from),
                Criteria::expr()->lte('result.status', $to)
            ));
            return $criteria;
        };

        return $this->stream('Project/show.html.twig', [
            'project' => $project,
            'statusUrls' => [
                '5xx' => $urlRepository->createList($project, null, $statusCriteria(500, 599)),
                '4xx' => $urlRepository->createList($project, null, $statusCriteria(400, 499)),
                '3xx' => $urlRepository->createList($project, null, $statusCriteria(300, 399)),
                '2xx' => $urlRepository->createList($project, null, $statusCriteria(200, 299)),
                '1xx' => $urlRepository->createList($project, null, $statusCriteria(100, 199)),
            ],
        ]);
    }
}