<?php


namespace App\Doctrine\Entity;


use Doctrine\ORM\Mapping as ORM;

trait EntityFields
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return get_class($this) . ':' . $this->getId();
    }
}