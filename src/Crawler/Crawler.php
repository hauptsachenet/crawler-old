<?php

namespace App\Crawler;


use App\Crawler\Entity\Result;
use App\Crawler\Entity\Url;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;

class Crawler
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Url $url
     * @return Result
     * @throws CrawlerException
     */
    public function crawl(Url $url): Result
    {
        try {
            $url->crawlAfterMinimumTime();
            $response = $this->client->request('GET', $url->getValue(), [
                'allow_redirects' => false
            ]);
        } catch (BadResponseException $e) {
            $response = $e->getResponse();
        } catch (GuzzleException $e) {
            throw new CrawlerException("crawling of $url failed.", 0, $e);
        }

        return new Result($url, $response);
    }
}