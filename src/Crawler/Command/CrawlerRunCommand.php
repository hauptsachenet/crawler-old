<?php

namespace App\Crawler\Command;


use App\Crawler\CrawlerException;
use App\Crawler\Entity\Parser\Reference;
use App\Crawler\Entity\Project;
use App\Crawler\Entity\Url;
use App\Crawler\Parser;
use App\Crawler\Repository\ProjectRepository;
use App\Crawler\Repository\UrlRepository;
use App\Crawler\Crawler;
use Doctrine\Common\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CrawlerRunCommand extends Command
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var UrlRepository
     */
    private $urlRepository;

    /**
     * @var Crawler
     */
    private $crawler;

    /**
     * @var Parser
     */
    private $parser;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(ObjectManager $objectManager, Crawler $crawler, Parser $parser, LoggerInterface $logger)
    {
        parent::__construct();
        $this->objectManager = $objectManager;
        $this->projectRepository = $objectManager->getRepository(Project::class);
        $this->urlRepository = $objectManager->getRepository(Url::class);
        $this->crawler = $crawler;
        $this->parser = $parser;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setName('crawler:run');
        $this->addOption('timeout', 't', InputOption::VALUE_REQUIRED, "timeout until the crawler will stop", 30);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $project = $this->projectRepository->findNextToCrawl();

        if (!$output->isQuiet()) {
            $output->writeln("crawl project $project");

            $parserCount = count($this->parser->getParsers());
            $output->writeln("will use <info>$parserCount</info> parsers");

            $count = $this->urlRepository->countUrlsToCrawl($project);
            $output->writeln("found <info>$count</info> urls to crawl");
        }

        $timeout = time() + $input->getOption('timeout');
        while (true) {
            $memoryBefore = memory_get_usage();
            if ($memoryBefore > 1e9) {
                $output->writeln("memory limit reached");
                return 0;
            }

            if (time() >= $timeout) {
                $output->writeln("timeout reached");
                return 0;
            }

            $url = $this->urlRepository->findUrlToCrawl($project);
            if (!$url) {
                $output->writeln("nothing to do");
                return 10;
            }

            try {

                $output->write("Crawl url <info>" . $url->getValue() . "</info>... ");

                $result = $this->crawler->crawl($url);
                $this->objectManager->persist($result);

                $response = $result->getResponse();
                $statusCode = $response->getStatusCode();
                $reasonPhrase = $response->getReasonPhrase();
                $output->write("<info>$statusCode</info> $reasonPhrase... ");

                $parsingResults = $this->parser->parse($result);
                foreach ($parsingResults as $parsingResult) {
                    $this->objectManager->persist($parsingResult);
                }

                $referenceCount = count(array_filter($parsingResults, function ($parsingResult) {
                    return $parsingResult instanceof Reference;
                }));
                $output->write("found <info>$referenceCount</info> references... ");

            } catch (CrawlerException $e) {
                $output->write($e->getMessage());
            }

            $this->objectManager->flush();
            $output->writeln("done");
            $this->objectManager->clear();
            unset($url, $parsingResults);

            $memoryAfter = memory_get_usage();
            $this->logger->debug("crawled url", [
                'before' => $memoryBefore,
                'after' => $memoryAfter,
                'diff' => $memoryAfter - $memoryBefore
            ]);
        }

        return 1; // this shouldn't be reachable
    }
}