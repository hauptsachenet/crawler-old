<?php

namespace App\Crawler\Command;


use App\Crawler\Repository\ProjectRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CrawlerProjectList extends Command
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    public function __construct(ProjectRepository $projectRepository)
    {
        parent::__construct();
        $this->projectRepository = $projectRepository;
    }

    protected function configure()
    {
        $this->setName('crawler:project:list');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $projects = $this->projectRepository->createList();
        if (empty($projects)) {
            $output->writeln("no projects found");
            return 10;
        }

        $table = new Table($output);
        $table->setHeaders(array_keys(reset($projects)));
        $table->setRows(array_map(function (array $project) {
            $project['baseUrls'] = implode("\n", $project['baseUrls']);
            return $project;
        }, $projects));
        $table->render();
    }
}