<?php

namespace App\Crawler\Command;


use App\Crawler\Entity\Project;
use App\Crawler\Entity\Url;
use App\Crawler\Factory\UrlFactory;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CrawlerProjectCreateCommand extends Command
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var UrlFactory
     */
    private $urlFactory;

    public function __construct(ObjectManager $objectManager, UrlFactory $urlFactory)
    {
        parent::__construct();
        $this->objectManager = $objectManager;
        $this->urlFactory = $urlFactory;
    }

    protected function configure()
    {
        $this->setName('crawler:project:create');
        $this->addArgument('url', InputArgument::REQUIRED, "The url to crawl");
        $this->addOption('base-url', 'b', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, "Base urls. If defined will replace url as base");
        $this->addOption('exclude-url', 'x', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, "Exclude urls.");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $project = new Project();
        $url = $input->getArgument('url');
        $project->setStartingPoint($this->urlFactory->get($project, $url));

        $baseUrls = $input->getOption('base-url');
        if (empty($baseUrls)) {
            $project->addBaseUrl($url);
        } else {
            foreach ($baseUrls as $baseUrl) {
                $project->addBaseUrl($baseUrl);
            }
        }

        $excludeUrls = $input->getOption('exclude-url');
        foreach ($excludeUrls as $excludeUrl) {
            $project->addExcludeUrl($excludeUrl);
        }

        $this->objectManager->persist($project);
        $this->objectManager->flush();
        $output->writeln("created project <info>$project</info>");
    }
}