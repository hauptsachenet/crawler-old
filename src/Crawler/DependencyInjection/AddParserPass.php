<?php

namespace App\Crawler\DependencyInjection;


use App\Crawler\Parser;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class AddParserPass implements CompilerPassInterface
{
    const TAG = 'crawler.parser';

    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        $parserServices = $container->findTaggedServiceIds(self::TAG, true);
        $crawlerService = $container->getDefinition(Parser::class);
        foreach ($parserServices as $serviceId => $tag) {
            $crawlerService->addArgument(new Reference($serviceId));
        }
    }
}