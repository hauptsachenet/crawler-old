<?php

namespace App\Crawler\Factory;


use App\Crawler\Entity\Project;
use App\Crawler\Entity\Url;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Events;
use Psr\Log\LoggerInterface;

class UrlFactory
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var Url[]
     */
    private $instances = [];

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(ObjectManager $objectManager, LoggerInterface $logger)
    {
        $this->objectManager = $objectManager;
        $this->logger = $logger;
        $this->registerClearEvent();
    }

    private function registerClearEvent()
    {
        if (!$this->objectManager instanceof EntityManager) {
            return;
        }

        $listener = new class($this->instances, $this->logger)
        {
            private $instances;
            private $logger;

            public function __construct(array &$instances, LoggerInterface $logger)
            {
                $this->instances =& $instances;
                $this->logger = $logger;
            }

            public function onClear()
            {
                $memoryBefore = memory_get_usage();
                $this->instances = [];
                $memoryAfter = memory_get_usage();
                $this->logger->debug("clear, url instances", [
                    'before' => $memoryBefore,
                    'after' => $memoryAfter,
                    'saved' => $memoryBefore - $memoryAfter
                ]);
            }
        };

        $this->objectManager->getEventManager()->addEventListener(Events::onClear, $listener);
    }

    /**
     * Creates a new url instance or gives you the already existing instance if available.
     *
     * @param Project $project
     * @param string $url
     * @return Url
     */
    public function get(Project $project, string $url): Url
    {
        $hash = spl_object_hash($project) . ':' . $url;

        if (isset($this->instances[$hash])) {
            $instance = $this->instances[$hash];

            $objectIsPersisted = $instance->getId() !== null;
            $objectIsManaged = $this->objectManager->contains($instance);
            if (!$objectIsPersisted || $objectIsManaged) {
                return $instance;
            } else {
                unset($this->instances[$hash]);
            }
        }

        $instance = $this->objectManager->getRepository(Url::class)->findOneBy(['project' => $project, 'value' => $url]);
        if ($instance === null) {
            $instance = new Url($project, $url);
        }

        $this->instances[$hash] = $instance;
        return $instance;
    }
}