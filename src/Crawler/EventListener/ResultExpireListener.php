<?php

namespace App\Crawler\EventListener;


use App\Crawler\Entity\Result;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;

class ResultExpireListener
{
    public function prePersist(Result $result, LifecycleEventArgs $event)
    {
        $objectManager = $event->getObjectManager();
        if (!$objectManager instanceof EntityManager) {
            $type = is_object($objectManager) ? get_class($objectManager) : gettype($objectManager);
            throw new \RuntimeException("Expected EntityManager, got $type");
        }

        $this->expireOldResults($result, $objectManager);
        $this->setExpireDate($result, $objectManager);
    }

    protected function setExpireDate(Result $result, EntityManager $objectManager): void
    {
        $isNewerCriteria = Criteria::create()->where(Criteria::expr()->andX(
            Criteria::expr()->eq('url', $result->getUrl()),
            Criteria::expr()->gt('createdAt', $result->getCreatedAt())
        ));

        foreach ($this->getResultsByCriteria($isNewerCriteria, $objectManager) as $newerResult) {
            if ($result->getExpiredAt() === null || $result->getExpiredAt() > $newerResult->getCreatedAt()) {
                $result->setExpiredAt($newerResult->getCreatedAt());
            }
        }
    }

    protected function expireOldResults(Result $result, EntityManager $objectManager): void
    {
        $mustExpireCriteria = Criteria::create()->where(Criteria::expr()->andX(
            Criteria::expr()->eq('url', $result->getUrl()),
            Criteria::expr()->lte('createdAt', $result->getCreatedAt()),
            Criteria::expr()->orX(
                Criteria::expr()->isNull('expiredAt'),
                Criteria::expr()->gt('expiredAt', $result->getCreatedAt())
            )
        ));

        foreach ($this->getResultsByCriteria($mustExpireCriteria, $objectManager) as $olderResult) {
            $olderResult->setExpiredAt($result->getCreatedAt());
        }
    }

    /**
     * @param Criteria $criteria
     * @param EntityManager $entityManager
     * @return \Generator|Result[]
     */
    protected function getResultsByCriteria(Criteria $criteria, EntityManager $entityManager): \Iterator
    {
        $instanceOfResult = function ($entity) {
            return $entity instanceof Result;
        };

        $scheduledForInsertion = new ArrayCollection($entityManager->getUnitOfWork()->getScheduledEntityInsertions());
        yield from $scheduledForInsertion->filter($instanceOfResult)->matching($criteria);
        yield from $entityManager->getRepository(Result::class)->matching($criteria);
    }
}