<?php

namespace App\Crawler\Entity;


use App\Doctrine\Entity\EntityFields;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Crawler\Repository\UrlRepository")
 * @ORM\Table(name="crawler_url",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="value", columns={"project_id", "value"})},
 *     indexes={@ORM\Index(name="next", columns={"next_crawl"})}
 * )
 */
class Url
{
    use EntityFields;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project")
     */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(type="binary_string", length=3000)
     */
    private $value;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $nextCrawl;

    public function __construct(Project $project, string $value)
    {
        $this->project = $project;
        $this->value = $value;
        $this->nextCrawl = new \DateTimeImmutable();
    }

    public function getProject(): Project
    {
        return $this->project;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getNextCrawl(): \DateTimeImmutable
    {
        return $this->nextCrawl;
    }

    public function setNextCrawl(\DateTimeInterface $nextCrawl): void
    {
        if ($nextCrawl instanceof \DateTime) {
            $nextCrawl = \DateTimeImmutable::createFromMutable($nextCrawl);
        }

        if ($nextCrawl < new \DateTimeImmutable($this->getProject()->getMinimumCrawlTimeout() . ' seconds')) {
            $nextCrawl = new \DateTimeImmutable($this->getProject()->getMinimumCrawlTimeout() . ' seconds');
        }

        if ($nextCrawl > new \DateTimeImmutable($this->getProject()->getMaximumCrawlTimeout() . ' seconds')) {
            $nextCrawl = new \DateTimeImmutable($this->getProject()->getMaximumCrawlTimeout() . ' seconds');
        }

        $this->nextCrawl = $nextCrawl;
    }

    public function crawlNext(): void
    {
        $this->nextCrawl = new \DateTimeImmutable();
    }

    public function crawlAfterMinimumTime(): void
    {
        $this->nextCrawl = new \DateTimeImmutable($this->getProject()->getMinimumCrawlTimeout() . ' seconds');
    }

    public function postponeNextCrawlUntil(\DateTimeInterface $dateTime): bool
    {
        if ($dateTime <= $this->nextCrawl) {
            return false;
        }

        $this->setNextCrawl($dateTime);
        return true;
    }
}