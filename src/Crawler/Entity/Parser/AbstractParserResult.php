<?php

namespace App\Crawler\Entity\Parser;

use App\Crawler\Entity\Result;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\MappedSuperclass()
 */
abstract class AbstractParserResult
{
    /**
     * @var Result
     *
     * @ORM\ManyToOne(targetEntity="App\Crawler\Entity\Result")
     */
    private $result;

    public function __construct(Result $result)
    {
        $this->result = $result;
    }

    public function getResult(): Result
    {
        return $this->result;
    }
}