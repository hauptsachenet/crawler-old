<?php

namespace App\Crawler\Entity\Parser;
use App\Crawler\Entity\Result;
use App\Crawler\Entity\Url;
use App\Doctrine\Entity\EntityFields;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity()
 * @ORM\Table(name="crawler_reference")
 */
class Reference extends AbstractParserResult
{
    use EntityFields;

    /**
     * @var Url
     *
     * @ORM\ManyToOne(targetEntity="App\Crawler\Entity\Url", cascade={"persist"})
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $label;

    public function __construct(Result $result, Url $url, string $label = '')
    {
        parent::__construct($result);
        $this->url = $url;
        $this->label = $label;
    }

    public function getUrl(): Url
    {
        return $this->url;
    }

    public function getLabel(): string
    {
        return $this->label;
    }
}