<?php

namespace App\Crawler\Entity;
use App\Doctrine\Entity\EntityFields;
use Doctrine\ORM\Mapping as ORM;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;


/**
 * @ORM\Entity(readOnly=true)
 * @ORM\Table(name="crawler_result", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="datetime", columns={"url_id", "created_at", "expired_at"})
 * })
 * @ORM\EntityListeners("App\Crawler\EventListener\ResultExpireListener")
 */
class Result
{
    use EntityFields;

    /**
     * @var Url
     *
     * @ORM\ManyToOne(targetEntity="Url")
     */
    private $url;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * To make queries easier, save also when this result expired because there was a newer result.
     * This value is automatically filled in by the persistence.
     *
     * @var \DateTimeImmutable|null
     * @internal used for easier queries
     *
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $expiredAt;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="json_array")
     */
    private $headers;

    /**
     * @var resource|StreamInterface
     *
     * @ORM\Column(type="blob")
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $protocolVersion;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $reason;

    /**
     * @var Response|null
     */
    private $_response;

    public function __construct(Url $url, ResponseInterface $response, \DateTimeImmutable $createdAt = null)
    {
        $this->url = $url;
        $this->createdAt = $createdAt ?? new \DateTimeImmutable();

        $this->status = $response->getStatusCode();
        $this->headers = $response->getHeaders();
        $this->body = $response->getBody();
        $this->protocolVersion = $response->getProtocolVersion();
        $this->reason = $response->getReasonPhrase();
        $this->_response = $response;
    }

    public function getProject(): Project
    {
        return $this->url->getProject();
    }

    public function getUrl(): Url
    {
        return $this->url;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getExpiredAt(): ?\DateTimeImmutable
    {
        return $this->expiredAt;
    }

    public function setExpiredAt(\DateTimeImmutable $expiredAt): void
    {
        $this->expiredAt = $expiredAt;
    }

    public function getResponse(): ResponseInterface
    {
        if ($this->_response === null) {
            $this->_response = new Response(
                $this->status,
                $this->headers,
                $this->body,
                $this->protocolVersion,
                $this->reason
            );
        }

        return $this->_response;
    }
}