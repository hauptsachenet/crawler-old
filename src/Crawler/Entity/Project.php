<?php

namespace App\Crawler\Entity;
use App\Doctrine\Entity\EntityFields;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Crawler\Repository\ProjectRepository")
 * @ORM\Table(name="crawler_project")
 */
class Project
{
    use EntityFields;

    /**
     * @var string[]
     *
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $baseUrls = [];

    /**
     * @var string[]
     *
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $excludeUrls = [];

    /**
     * @var Url|null
     *
     * @ORM\OneToOne(targetEntity="App\Crawler\Entity\Url", cascade={"PERSIST"})
     */
    private $startingPoint;

    public function getBaseUrls(): array
    {
        return $this->baseUrls;
    }

    public function addBaseUrl(string $url): void
    {
        $this->removeBaseUrl($url);
        $this->baseUrls[] = $url;
    }

    public function removeBaseUrl(string $url): void
    {
        $index = array_search($url, $this->baseUrls, true);
        if ($index !== false) {
            array_splice($this->baseUrls, $index, 1);
        }
    }

    public function getExcludeUrls(): array
    {
        return $this->excludeUrls;
    }

    public function addExcludeUrl(string $url): void
    {
        $this->removeExcludeUrl($url);
        $this->excludeUrls[] = $url;
    }

    public function removeExcludeUrl(string $url): void
    {
        $index = array_search($url, $this->excludeUrls, true);
        if ($index !== false) {
            array_splice($this->excludeUrls, $index, 1);
        }
    }

    public function getStartingPoint(): ?Url
    {
        return $this->startingPoint;
    }

    public function setStartingPoint(?Url $startingPoint): void
    {
        if ($startingPoint !== null && $startingPoint->getProject() !== $this) {
            throw new \LogicException("Starting point $startingPoint must belong to the $this");
        }

        $this->startingPoint = $startingPoint;
    }

    public function getMinimumCrawlTimeout(): int
    {
        return 60 * 60;
    }

    public function getMaximumCrawlTimeout(): int
    {
        return 60 * 60 * 24 * 7;
    }
}