<?php

namespace App\Crawler\Repository;


use App\Crawler\Entity\Project;
use App\Crawler\Entity\Result;
use App\Crawler\Entity\Url;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

class UrlRepository extends EntityRepository
{
    protected function createTodoQueryBuilder(string $alias, Project $project): QueryBuilder
    {
        $qb = $this->createQueryBuilder($alias);

        $qb->setParameter('now', new \DateTime('now'));
        $qb->andWhere("$alias.nextCrawl <= :now");
        $qb->addOrderBy("$alias.nextCrawl", 'ASC');

        $urlExpr = $qb->expr()->orX();
        foreach ($project->getBaseUrls() as $index => $baseUrl) {
            $qb->setParameter("base_url_$index", "$baseUrl%");
            $urlExpr->add("$alias.value LIKE :base_url_$index");
        }
        if ($urlExpr->count() > 0) {
            $qb->andWhere($urlExpr);
        }

        foreach ($project->getExcludeUrls() as $index => $excludeUrl) {
            $qb->setParameter("exclude_url_$index", "$excludeUrl%");
            $qb->andWhere("$alias.value NOT LIKE :exclude_url_$index");
        }

        return $qb;
    }

    public function findUrlToCrawl(Project $project): ?Url
    {
        $qb = $this->createTodoQueryBuilder('url', $project);
        $qb->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function countUrlsToCrawl(Project $project): int
    {
        $qb = $this->createTodoQueryBuilder('url', $project);
        $qb->select('COUNT(url.id)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    public function createList(Project $project, \DateTimeInterface $date = null, Criteria $criteria = null): array
    {
        $qb = $this->createQueryBuilder('url');
        $qb->where('url.project = :project');
        $qb->setParameter('project', $project);
        $qb->select('url.value');

        $qb->setParameter('date', $date ?? new \DateTimeImmutable());
        $qb->leftJoin(Result::class, 'result', Join::WITH, $qb->expr()->andX(
            'result.url = url',
            'result.createdAt <= :date',
            'result.expiredAt > :date OR result.expiredAt IS NULL'
        ));
        $qb->addSelect('result.status AS lastStatus');
        $qb->addSelect('result.createdAt AS lastCrawl');
        $qb->addSelect('url.nextCrawl AS nextCrawl');

        if ($criteria !== null) {
            $qb->addCriteria($criteria);
        }

        return $qb->getQuery()->getArrayResult();
    }
}