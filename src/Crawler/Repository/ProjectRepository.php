<?php

namespace App\Crawler\Repository;


use App\Crawler\Entity\Result;
use App\Crawler\Entity\Url;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class ProjectRepository extends EntityRepository
{
    public function findNextToCrawl()
    {
        return $this->findOneBy([]);
    }

    public function createList(Criteria $criteria = null, \DateTimeInterface $date = null): array
    {
        $qb = $this->createQueryBuilder('project');
        $qb->addSelect('project.id');
        $qb->addSelect('project.baseUrls');
        $qb->addSelect('project.excludeUrls');

        $qb->leftJoin('project.startingPoint', 'startingUrl');
        $qb->addSelect('startingUrl.value AS startingPoint');

        $qb->leftJoin(Url::class, 'url', Join::WITH, 'url.project = project');
        $qb->addSelect('COUNT(url.id) AS urlCount');

        $qb->setParameter('date', $date ?? new \DateTimeImmutable());
        $qb->leftJoin(Result::class, 'result', Join::WITH, $qb->expr()->andX(
            'result.url = url',
            'result.createdAt <= :date',
            'result.expiredAt > :date OR result.expiredAt IS NULL'
        ));
        $qb->addSelect('COUNT(result.id) AS crawledUrlCount');
        $qb->addSelect('SUM(CASE WHEN result.status >= 100 AND result.status <= 199 THEN 1 ELSE 0 END) AS result100');
        $qb->addSelect('SUM(CASE WHEN result.status >= 200 AND result.status <= 299 THEN 1 ELSE 0 END) AS result200');
        $qb->addSelect('SUM(CASE WHEN result.status >= 300 AND result.status <= 399 THEN 1 ELSE 0 END) AS result300');
        $qb->addSelect('SUM(CASE WHEN result.status >= 400 AND result.status <= 499 THEN 1 ELSE 0 END) AS result400');
        $qb->addSelect('SUM(CASE WHEN result.status >= 500 AND result.status <= 599 THEN 1 ELSE 0 END) AS result500');

        $qb->groupBy('project.id');
        return $qb->getQuery()->getResult();
    }
}