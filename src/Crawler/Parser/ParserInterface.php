<?php

namespace App\Crawler\Parser;


use App\Crawler\Entity\Result;

interface ParserInterface
{
    public function parse(Result $result): array;
}