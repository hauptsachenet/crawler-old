<?php

namespace App\Crawler\Parser;


use App\Crawler\Entity\Parser\Reference;
use App\Crawler\Entity\Result;
use App\Crawler\Factory\UrlFactory;

class RedirectParser implements ParserInterface
{
    /**
     * @var UrlFactory
     */
    private $urlFactory;

    public function __construct(UrlFactory $urlFactory)
    {
        $this->urlFactory = $urlFactory;
    }

    public function parse(Result $result): array
    {
        $parsingResults = [];

        $baseUrl = parse_url($result->getUrl()->getValue());
        unset($baseUrl['path']); // it seems like HTTP_URL_JOIN_PATH is badly implemented ~ use Path::join later
        unset($baseUrl['query']); // it seems like HTTP_URL_STRIP_QUERY is not implemented
        unset($baseUrl['fragment']); // it seems like HTTP_URL_STRIP_FRAGMENT is not implemented

        foreach ($result->getResponse()->getHeader('Location') as $location) {
            $urlString = http_build_url($baseUrl, $location);
            $url = $this->urlFactory->get($result->getProject(), $urlString);
            $parsingResults[] = new Reference($result, $url);
        }

        return $parsingResults;
    }
}