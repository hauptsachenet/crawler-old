<?php

namespace App\Crawler\Parser;


use App\Crawler\Entity\Result;

class ReCrawlAfterParser implements ParserInterface
{
    public function parse(Result $result): array
    {
        $response = $result->getResponse();
        $possibleExpireDates = [];

        foreach ($response->getHeader('Retry-After') as $retryAfter) {
            $possibleExpireDates[] = $result->getCreatedAt()->modify("$retryAfter seconds");
        }

        $requestSuccessful = $response->getStatusCode() >= 200 && $response->getStatusCode() <= 299;
        if ($requestSuccessful) {
            foreach ($response->getHeader('Cache-Control') as $cacheControl) {
                if (preg_match('#max-age=(\d+)#', $cacheControl, $match)) {
                    $possibleExpireDates[] = $result->getCreatedAt()->modify("{$match[1]} seconds");
                }
            }

            foreach ($response->getHeader('Expires') as $expires) {
                try {
                    $possibleExpireDates[] = new \DateTimeImmutable($expires);
                } catch (\Exception $e) {
                }
            }
        }

        if (empty($possibleExpireDates)) {
            return [];
        }

        $result->getUrl()->postponeNextCrawlUntil(min($possibleExpireDates));
        return [];
    }
}