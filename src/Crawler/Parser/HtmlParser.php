<?php

namespace App\Crawler\Parser;


use App\Crawler\Entity\Parser\Reference;
use App\Crawler\Entity\Result;
use App\Crawler\Factory\UrlFactory;

class HtmlParser implements ParserInterface
{
    private const REFERENCE_REGEX = '#<[^>]*(?:src|href)\s*=\s*[\'"]([^\'"]+)[\'"][^>]*>#ui';

    /**
     * @var UrlFactory
     */
    private $urlFactory;

    public function __construct(UrlFactory $urlFactory)
    {
        $this->urlFactory = $urlFactory;
    }

    /**
     * @param Result $result
     * @return Reference[]
     */
    public function parse(Result $result): array
    {
        $response = $result->getResponse();
        $contentType = $response->getHeaderLine('Content-Type');
        if (!preg_match('#^text/x?html|^application/xhtml\+xml#i', $contentType)) {
            return [];
        }

        $matches = [/* filled by next line */];
        if (!preg_match_all(self::REFERENCE_REGEX, $response->getBody(), $matches, PREG_SET_ORDER)) {
            return [];
        }

        $baseUrl = parse_url($result->getUrl()->getValue());
        unset($baseUrl['path']); // it seems like HTTP_URL_JOIN_PATH is badly implemented ~ use Path::join later
        unset($baseUrl['query']); // it seems like HTTP_URL_STRIP_QUERY is not implemented
        unset($baseUrl['fragment']); // it seems like HTTP_URL_STRIP_FRAGMENT is not implemented

        $references = [];

        foreach ($matches as $match) {
            $urlString = http_build_url($baseUrl, html_entity_decode($match[1]));
            $url = $this->urlFactory->get($result->getProject(), $urlString);
            $references[] = new Reference($result, $url, $match[0]);
        }

        return $references;
    }
}