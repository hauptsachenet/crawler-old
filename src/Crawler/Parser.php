<?php

namespace App\Crawler;


use App\Crawler\Entity\Result;
use App\Crawler\Parser\ParserInterface;

class Parser
{
    /**
     * @var ParserInterface[]
     */
    private $parsers;

    /**
     * @param ParserInterface[] $parsers
     */
    public function __construct(ParserInterface ...$parsers)
    {
        $this->parsers = $parsers;
    }

    /**
     * @param Result $result
     * @return array
     */
    public function parse(Result $result): array
    {
        $return = [];

        foreach ($this->parsers as $parser) {
            foreach ($parser->parse($result) as $parseItem) {
                $return[] = $parseItem;
            }
        }

        return $return;
    }

    public function getParsers(): array
    {
        return $this->parsers;
    }
}