<?php

namespace App\Tests\Crawler\EventListener;


use App\Crawler\Entity\Project;
use App\Crawler\Entity\Result;
use App\Crawler\Entity\Url;
use App\Tests\DatabaseTest;
use GuzzleHttp\Psr7\Response;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ResultExpireListener extends KernelTestCase
{
    use DatabaseTest;

    public static function expireSets()
    {
        $date12 = new \DateTimeImmutable('12:00:00');
        $date13 = new \DateTimeImmutable('13:00:00');

        return [
            [
                [$date12, $date13],
                [$date13, null],
            ],
            [
                [$date13, $date12],
                [null, $date13],
            ],
        ];
    }

    /**
     * @dataProvider expireSets
     * @param \DateTimeImmutable[] $createdAtDates
     * @param \DateTimeImmutable[] $expireDates
     */
    public function testSetExpired(array $createdAtDates, array $expireDates)
    {
        $em = self::createEntityManager(self::bootKernel());
        $project = new Project();
        $url = new Url($project, 'http://www.example.com/');

        /** @var Result[] $results */
        $results = [];
        foreach ($createdAtDates as $index => $createdAtDate) {
            $results[$index] = new Result($url, new Response(), $createdAtDate);
        }

        foreach ($results as $index => $result) {
            $this->assertNull($result->getExpiredAt(), $index);
        }

        foreach ($results as $result) {
            $em->persist($result);
        }

        foreach ($results as $index => $result) {
            $this->assertEquals($expireDates[$index], $result->getExpiredAt(), $index);
        }
    }
}