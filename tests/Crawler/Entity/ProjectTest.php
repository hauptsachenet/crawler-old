<?php

namespace App\Crawler\Entity;

use PHPUnit\Framework\TestCase;

class ProjectTest extends TestCase
{
    public function testBaseUrls()
    {
        $project = new Project();
        $this->assertCount(0, $project->getBaseUrls());
        $project->addBaseUrl('http://www.example.com/');
        $this->assertCount(1, $project->getBaseUrls());
        $project->addBaseUrl('http://www.example.com/');
        $this->assertCount(1, $project->getBaseUrls());
        $project->removeBaseUrl('http://www.example.com/');
        $this->assertCount(0, $project->getBaseUrls());
    }
    public function testExcludeUrls()
    {
        $project = new Project();
        $this->assertCount(0, $project->getExcludeUrls());
        $project->addExcludeUrl('http://www.example.com/');
        $this->assertCount(1, $project->getExcludeUrls());
        $project->addExcludeUrl('http://www.example.com/');
        $this->assertCount(1, $project->getExcludeUrls());
        $project->removeExcludeUrl('http://www.example.com/');
        $this->assertCount(0, $project->getExcludeUrls());
    }

    public function testStartingPoint()
    {
        $project = new Project();
        $this->assertEquals(null, $project->getStartingPoint());

        $url = new Url($project, 'http://www.example.com/');
        $project->setStartingPoint($url);
        $this->assertSame($url, $project->getStartingPoint());
    }

    /**
     * @expectedException \LogicException
     */
    public function testBadStartingPoint()
    {
        $project = new Project();

        $url = new Url(new Project(), 'http://www.example.com');
        $project->setStartingPoint($url);
    }
}
