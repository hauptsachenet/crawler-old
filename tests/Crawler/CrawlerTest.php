<?php

namespace App\Crawler;

use App\Crawler\Entity\Crawl;
use App\Crawler\Entity\Project;
use App\Crawler\Entity\Url;
use App\Crawler\Parser\HtmlParser;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class CrawlerTest extends TestCase
{
    public function testCrawl()
    {
        $client = $this->createMock(Client::class);
        $client->expects($this->once())
            ->method('request')
            ->with($this->equalTo('GET'), $this->equalTo('http://www.example.com/'))
            ->willReturn(new Response(200, [], "Response"))
        ;

        $project = new Project();
        $project->addBaseUrl('http://www.example.com/');
        $url = new Url($project, 'http://www.example.com/');

        $crawler = new Crawler($client);
        $result = $crawler->crawl($url);
        $this->assertEquals(200, $result->getResponse()->getStatusCode());
        $this->assertEquals("Response", $result->getResponse()->getBody()->__toString());
    }
}
