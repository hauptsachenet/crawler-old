<?php

namespace App\Crawler\Parser;

use App\Tests\Crawler\Parser\ParserTest;
use GuzzleHttp\Psr7\Response;

class RedirectParserTest extends ParserTest
{
    /**
     * @var RedirectParser
     */
    protected $redirectParser;

    public function setUp(): void
    {
        $this->redirectParser = self::bootKernel()->getContainer()->get(RedirectParser::class);
    }

    public function testNoneRedirect()
    {
        $response = new Response(200, [], '');
        $result = $this->redirectParser->parse($this->createCrawlerResult($response));
        $this->assertCount(0, $result);
    }

    public function testRedirect()
    {
        $response = new Response(200, ['Location' => '/foo'], '');
        $result = $this->redirectParser->parse($this->createCrawlerResult($response));
        $this->assertCount(1, $result);
        $this->assertEquals('http://www.example.com/foo', $result[0]->getUrl()->getValue());
    }

    public function testRedirectAbsolute()
    {
        $response = new Response(200, ['Location' => 'https://example.com/foo'], '');
        $result = $this->redirectParser->parse($this->createCrawlerResult($response));
        $this->assertCount(1, $result);
        $this->assertEquals('https://example.com/foo', $result[0]->getUrl()->getValue());
    }
}
