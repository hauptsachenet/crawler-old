<?php

namespace App\Crawler\Parser;

use App\Crawler\Factory\UrlFactory;
use App\Tests\Crawler\Parser\ParserTest;
use GuzzleHttp\Psr7\Response;

class HtmlParserTest extends ParserTest
{
    /**
     * @var HtmlParser
     */
    protected $htmlParser;

    public function setUp(): void
    {
        $this->htmlParser = self::bootKernel()->getContainer()->get(HtmlParser::class);
    }

    public function testEmptyParse()
    {
        $response = new Response(200, [], '');
        $references = $this->htmlParser->parse($this->createCrawlerResult($response));
        $this->assertEmpty($references);
    }

    public function testHtmlLinkOutsideHtml()
    {
        $response = new Response(200, [], '<a href="http://www.example.com">link</a>');
        $references = $this->htmlParser->parse($this->createCrawlerResult($response));
        $this->assertEmpty($references);
    }

    public function testHtmlLink()
    {
        $response = new Response(200, ['Content-Type' => ['text/html']], '<a href="http://www.example.com">link</a>');
        $references = $this->htmlParser->parse($this->createCrawlerResult($response));
        $this->assertCount(1, $references);
        $this->assertEquals('http://www.example.com', $references[0]->getUrl()->getValue());
        $this->assertEquals('<a href="http://www.example.com">', $references[0]->getLabel()); // not ideal
    }

    public static function mimeTypes()
    {
        return [
            [1, 'text/html'],
            [1, 'text/html; charset=utf-8'],
            [1, 'text/xhtml'],
            [1, 'application/xhtml+xml'],
            [0, 'text/plain'],
        ];
    }

    /**
     * @dataProvider mimeTypes
     */
    public function testDifferentMimeTypes($expect, $mimeType)
    {
        $response = new Response(200, ['Content-Type' => [$mimeType]], '<a href="http://www.example.com">link</a>');
        $references = $this->htmlParser->parse($this->createCrawlerResult($response));
        $this->assertCount($expect, $references, $mimeType);
    }

}
