<?php

namespace App\Crawler\Parser;

use App\Crawler\Entity\Project;
use App\Crawler\Entity\Result;
use App\Crawler\Entity\Url;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class ReCrawlAfterParserTest extends TestCase
{
    public function testEmpty()
    {
        $project = new Project();
        $url = new Url($project, 'http://www.example.com/');
        $oldDateTime = $url->getNextCrawl();
        $result = new Result($url, new Response());

        $reCrawlParser = new ReCrawlAfterParser();
        $this->assertEmpty($reCrawlParser->parse($result));
        $this->assertEquals($oldDateTime, $url->getNextCrawl());
    }

    public function testRetryAfter()
    {
        $project = new Project();
        $url = new Url($project, 'http://www.example.com/');
        $result = new Result($url, new Response(200, ['Retry-After' => [60 * 60 * 48]]));

        $reCrawlParser = new ReCrawlAfterParser();
        $this->assertEmpty($reCrawlParser->parse($result));
        $this->assertEquals($result->getCreatedAt()->modify("+2 days"), $url->getNextCrawl());
    }

    public function testExpires()
    {
        $project = new Project();
        $url = new Url($project, 'http://www.example.com/');
        $dateTime = new \DateTimeImmutable('+2 days');
        $result = new Result($url, new Response(200, ['Expires' => [$dateTime->format('r')]]));

        $reCrawlParser = new ReCrawlAfterParser();
        $this->assertEmpty($reCrawlParser->parse($result));
        $this->assertEquals($dateTime->format('c'), $url->getNextCrawl()->format('c'));
    }
}
