<?php

namespace App\Tests\Crawler\Parser;


use App\Crawler\Entity\Project;
use App\Crawler\Entity\Result;
use App\Crawler\Entity\Url;
use App\Crawler\Repository\UrlRepository;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class ParserTest extends KernelTestCase
{
    protected function createCrawlerResult(ResponseInterface $response): Result
    {
        $project = new Project();
        return new Result(new Url($project, 'http://www.example.com/'), $response);
    }

    protected function createUrlRepository(): UrlRepository
    {
        $urlRepository = $this->createMock(UrlRepository::class);
        $urlRepository->method('get')->willReturnCallback(function (Project $project, string $url) {
            return new Url($project, $url);
        });

        return $urlRepository;
    }
}