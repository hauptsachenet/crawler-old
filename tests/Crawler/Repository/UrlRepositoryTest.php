<?php

namespace App\Crawler\Repository;

use App\Crawler\Entity\Project;
use App\Crawler\Entity\Url;
use App\Crawler\Factory\UrlFactory;
use App\Tests\DatabaseTest;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UrlRepositoryTest extends KernelTestCase
{
    use DatabaseTest;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var UrlRepository
     */
    protected $urlRepository;

    public function setUp()
    {
        $this->entityManager = self::createEntityManager(self::bootKernel());
        $this->urlRepository = $this->entityManager->getRepository(Url::class);
    }

    public function testFindUrlToCrawl()
    {
        $project = new Project();
        $project->addBaseUrl('http://www.example.com/');
        $url = new Url($project, 'http://www.example.com/');
        $this->entityManager->persist($project);
        $this->entityManager->persist($url);
        $this->entityManager->flush();

        $foundUrl = $this->urlRepository->findUrlToCrawl($project);
        $this->assertSame($url, $foundUrl);
    }

    public function testBlacklist()
    {
        $project = new Project();
        $project->addBaseUrl('http://www.example.com/');
        $project->addExcludeUrl('http://www.example.com/temp');
        $url = new Url($project, 'http://www.example.com/temp');
        $this->entityManager->persist($project);
        $this->entityManager->persist($url);
        $this->entityManager->flush();

        $foundUrl = $this->urlRepository->findUrlToCrawl($project);
        $this->assertNull($foundUrl);
    }
}
