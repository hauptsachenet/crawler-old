<?php

namespace App\Tests\Crawler\Factory;


use App\Crawler\Entity\Project;
use App\Crawler\Entity\Url;
use App\Crawler\Factory\UrlFactory;
use App\Tests\DatabaseTest;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UrlFactoryTest extends KernelTestCase
{
    use DatabaseTest;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function setUp()
    {
        $this->entityManager = self::createEntityManager(self::bootKernel());
    }

    public function testGetUrlInstance()
    {
        $project = new Project();
        $urlFactory = new UrlFactory($this->entityManager);

        $url1 = $urlFactory->get($project, 'http://www.example.com/');
        $this->assertFalse($this->entityManager->contains($url1));

        $url2 = $urlFactory->get($project, 'http://www.example.com/');
        $this->assertSame($url1, $url2);
    }

    public function testClear()
    {
        $project = new Project();
        $urlFactory = new UrlFactory($this->entityManager);
        $this->entityManager->persist($project);

        $url = $urlFactory->get($project, 'http://www.example.com/');
        $this->assertSame($url, $urlFactory->get($project, 'http://www.example.com/'));
        $this->entityManager->persist($url);
        $this->entityManager->flush();
        $this->assertEquals([$url], $this->entityManager->getRepository(Url::class)->findAll());

        $this->entityManager->clear();
        $newUrlInstance = $urlFactory->get($project, 'http://www.example.com/');
        $this->assertNotSame($url, $newUrlInstance);
        $this->assertSame($url->getId(), $newUrlInstance->getId());
    }

    public function testDetach()
    {
        $project = new Project();
        $urlFactory = new UrlFactory($this->entityManager);
        $this->entityManager->persist($project);

        $url = $urlFactory->get($project, 'http://www.example.com/');
        $this->assertSame($url, $urlFactory->get($project, 'http://www.example.com/'));
        $this->entityManager->persist($url);
        $this->entityManager->flush();
        $this->assertEquals([$url], $this->entityManager->getRepository(Url::class)->findAll());

        $this->entityManager->detach($url);
        $this->assertNotSame($url, $urlFactory->get($project, 'http://www.example.com/'));
    }
}