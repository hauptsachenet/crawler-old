<?php


namespace App\Tests;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;

trait DatabaseTest
{

    protected function createEntityManager(\Symfony\Component\HttpKernel\KernelInterface $kernel): EntityManager
    {
        $entityManager = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $metadata = $entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool($entityManager);
        $schemaTool->createSchema($metadata);
        return $entityManager;
    }

}