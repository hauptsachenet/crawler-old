FROM chialab/php:7.1-apache

RUN sed -i 's#/var/www/html#/var/www/public#g' /etc/apache2/sites-enabled/000-default.conf \
 && a2enmod alias deflate expires headers rewrite \
 && echo 'opcache.enable_file_override=On' >> /usr/local/etc/php/conf.d/php.ini \
 && echo 'opcache.revalidate_freq=0' >> /usr/local/etc/php/conf.d/php.ini